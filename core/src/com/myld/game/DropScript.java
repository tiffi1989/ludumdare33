package com.myld.game;

import com.badlogic.ashley.core.Entity;
import com.uwsoft.editor.renderer.scripts.IScript;

public interface DropScript extends IScript{

	public void pickup();
	
	public Entity getEntity();

}
