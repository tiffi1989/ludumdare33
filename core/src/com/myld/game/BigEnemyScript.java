package com.myld.game;

import java.util.ArrayList;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.Timer.Task;
import com.sun.xml.internal.fastinfoset.algorithm.HexadecimalEncodingAlgorithm;
import com.uwsoft.editor.renderer.components.TransformComponent;
import com.uwsoft.editor.renderer.components.particle.ParticleComponent;
import com.uwsoft.editor.renderer.components.physics.PhysicsBodyComponent;
import com.uwsoft.editor.renderer.components.sprite.SpriteAnimationComponent;
import com.uwsoft.editor.renderer.components.sprite.SpriteAnimationStateComponent;
import com.uwsoft.editor.renderer.data.ParticleEffectVO;
import com.uwsoft.editor.renderer.data.SimpleImageVO;
import com.uwsoft.editor.renderer.data.SpriteAnimationVO;
import com.uwsoft.editor.renderer.factory.component.ParticleEffectComponentFactory;
import com.uwsoft.editor.renderer.factory.component.SimpleImageComponentFactory;
import com.uwsoft.editor.renderer.factory.component.SpriteComponentFactory;
import com.uwsoft.editor.renderer.scripts.IScript;
import com.uwsoft.editor.renderer.utils.ComponentRetriever;
import com.uwsoft.editor.renderer.utils.ItemWrapper;

import box2dLight.PointLight;

public class BigEnemyScript implements IScript {

	public BigEnemyScript(Game game) {
		this.game = game;
	}

	ItemWrapper wrapper;
	private PhysicsBodyComponent bodyComponent;
	private SpriteAnimationStateComponent animAtionStateComponent;
	private SpriteAnimationComponent animationComponent;
	boolean firstTime;
	private Game game;
	public Entity entity;
	private TransformComponent transformComponent;
	public boolean dead;
	private PointLight light;
	private Vector2 startPosition;
	private boolean goLeft, attacking, burning;
	private float time = 0;
	private float timeToThrow = 0;
	private float timeElapsed, miauTime, dropTime;
	private ArrayList<Entity> axes = new ArrayList<Entity>();
	private Axe axe;
	private float health = 150;
	private float burnTime;

	@Override
	public void init(Entity entity) {
		this.entity = entity;
		transformComponent = ComponentRetriever.get(entity, TransformComponent.class);

		bodyComponent = ComponentRetriever.get(entity, PhysicsBodyComponent.class);

		animAtionStateComponent = ComponentRetriever.get(entity, SpriteAnimationStateComponent.class);

		animationComponent = ComponentRetriever.get(entity, SpriteAnimationComponent.class);
		transformComponent.originX = transformComponent.originX + 30;

	}

	@Override
	public void act(float delta) {
		for (Entity entity : axes) {
			PhysicsBodyComponent body = ComponentRetriever.get(entity, PhysicsBodyComponent.class);

			body.body.getFixtureList().first().setSensor(true);
			body.body.setUserData(axe);

			Vector2 vector = game.player.body.getPosition().cpy().sub(body.body.getPosition());

			if (Math.random() >= 0.5f)
				body.body.applyForceToCenter(vector.nor().scl(5000, 3000), true);
			else
				body.body.applyForceToCenter(new Vector2(4000f * transformComponent.scaleX * -1, (float) (1000f * Math.random())), true);

		}

		axes.clear();
		timeElapsed += delta;
		dropTime += delta;

		if (health <= 0 && !dead)
			burn();

		if (burning) {
			burnTime += delta;
			if (burnTime > 0.1f) {
				health -= 10;
				burnTime = 0;
			}
		} else
			burnTime = 0;

		if (timeElapsed > 4 && isPlayerInRange() && !dead) {
			miauTime += (float) (delta * Math.random());
			if (miauTime > 2) {
				SoundHandler.mimimi.play();
				miauTime = 0;
			}
		}
		if (dropTime > 4 && !dead) {
			drop();
			dropTime = 0;

		}

		time += delta;
		timeToThrow += delta;

		if (!firstTime) {
			bodyComponent.body.setUserData(this);
			firstTime = true;
			startPosition = bodyComponent.body.getPosition().cpy();
			bodyComponent.body.setLinearVelocity(10f, 0);
			bodyComponent.body.setGravityScale(1f);
		}
		if (!dead)
			if (!attacking)
				moveEnemy();
			else
				attack();

		if (dead) {
			bodyComponent.body.getFixtureList().first().setRestitution(0);
			if (bodyComponent.body.getLinearVelocity().y <= 1 && bodyComponent.body.getLinearVelocity().y >= -1)
				game.bodiesToRemove.add(bodyComponent.body);

		}

		if (bodyComponent.body.getPosition().y <= -30)
			dead = true;
	}

	private void attack() {
		bodyComponent.body.setLinearVelocity(0, 0);

		if (timeToThrow > 0.85f / 2f) {
			createAxe();
			timeToThrow = 0;
		}
		if (animAtionStateComponent.currentAnimation.getAnimationDuration() < time) {
			time = 0;
			attacking = false;
			animAtionStateComponent.set(animationComponent.frameRangeMap.get("running"), 24, PlayMode.NORMAL);
			animationComponent.fps = 48;

		}

	}

	private void createAxe() {
		Entity entity = EntityCreator.createEntity(SpriteComponentFactory.class, SpriteAnimationVO.class, game, "axe");
		TransformComponent trans = entity.getComponent(TransformComponent.class);
		if (transformComponent.scaleX < 0)
			trans.x = transformComponent.x + 200;
		else
			trans.x = transformComponent.x;
		trans.y = transformComponent.y + 200;
		trans.scaleX = 1;
		trans.scaleY = 1;
		axes.add(entity);
		ItemWrapper wrapper = new ItemWrapper(entity);
		axe = new Axe(game);
		wrapper.addScript(axe);
		if (isPlayerInRange())
			SoundHandler.axt.play();

	}

	private void moveEnemy() {

		if (bodyComponent.body.getPosition().x <= startPosition.x - 20) {
			transformComponent.scaleX = -1f;
			goLeft = false;
		}
		if (bodyComponent.body.getPosition().x >= startPosition.x + 20) {
			goLeft = true;
			transformComponent.scaleX = 1f;
		}

		if (timeElapsed >= 2 && isPlayerInRange())
			if (bodyComponent.body.getPosition().x - game.player.body.getPosition().x > 0) {
				goLeft = true;
				transformComponent.scaleX = 1f;
			} else {

				transformComponent.scaleX = -1f;
				goLeft = false;
			}

		if (goLeft)
			bodyComponent.body.setLinearVelocity(-10f, 0);
		else
			bodyComponent.body.setLinearVelocity(10f, 0);

		if (animAtionStateComponent.currentAnimation.getAnimationDuration() < time) {
			time = 0;
			attacking = true;
			animAtionStateComponent.set(animationComponent.frameRangeMap.get("throwing"), 24, PlayMode.NORMAL);
			animationComponent.fps = 48;
			timeToThrow = 0;

		}

	}

	public void startBurn() {
		burning = true;
	}

	public void endBurn() {
		burning = false;
	}

	public void burn() {

		health -= 10;

		if (health > 0)
			return;
		animAtionStateComponent.set(animationComponent.frameRangeMap.get("burn"), 0, PlayMode.NORMAL);
		animationComponent.fps = 200;

		dead = true;

		Entity entity = EntityCreator.createEntity(ParticleEffectComponentFactory.class, ParticleEffectVO.class, game, "fire");
		TransformComponent trans = ComponentRetriever.get(entity, TransformComponent.class);
		trans.x = transformComponent.x + 170;
		trans.y = transformComponent.y + 110;

		light = new PointLight(game.sceneLoader.rayHandler, 100);
		light.setColor(com.badlogic.gdx.graphics.Color.WHITE);
		light.setXray(true);
		light.setDistance(20);
		light.setPosition(new Vector2(bodyComponent.body.getPosition().x + 5, bodyComponent.body.getPosition().y + 5));

		SoundHandler.tot.play();

	}

	private boolean isPlayerInRange() {
		Vector2 playerV = game.player.body.getPosition();
		Vector2 catV = bodyComponent.body.getPosition();

		if (playerV.dst(catV) < 80f)
			return true;
		else
			return false;
	}

	public void drop() {
		Entity entity = EntityCreator.createEntity(SimpleImageComponentFactory.class, SimpleImageVO.class, game, "fuel");
		TransformComponent trans = ComponentRetriever.get(entity, TransformComponent.class);
		trans.x = (float) (transformComponent.x + (Math.random() - 0.5f) * 500);
		trans.y = transformComponent.y + 200;
		ItemWrapper wrapper = new ItemWrapper(entity);
		wrapper.addScript(new FuelScript(game));
	}

	public void die() {
		animAtionStateComponent.set(animationComponent.frameRangeMap.get("dead"), 0, PlayMode.NORMAL);
		animationComponent.fps = 40;

		dead = true;

		Entity entity = EntityCreator.createEntity(ParticleEffectComponentFactory.class, ParticleEffectVO.class, game, "blood");
		TransformComponent trans = ComponentRetriever.get(entity, TransformComponent.class);
		trans.x = transformComponent.x + 170;
		trans.y = transformComponent.y + 110;

		ParticleComponent particle = ComponentRetriever.get(entity, ParticleComponent.class);
		particle.particleEffect.start();

		Task task = new Task() {

			@Override
			public void run() {
				// game.sceneLoader.getEngine().removeEntity(entity);
			}
		};
		Timer.schedule(task, 3);
		drop();

		SoundHandler.tot.play();
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}

	public void setItemWrapper(ItemWrapper wrapper) {
		this.wrapper = wrapper;
	}

}
