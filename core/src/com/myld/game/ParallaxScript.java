package com.myld.game;

import com.badlogic.ashley.core.Entity;
import com.uwsoft.editor.renderer.components.TransformComponent;
import com.uwsoft.editor.renderer.data.CompositeItemVO;
import com.uwsoft.editor.renderer.scripts.IScript;

public class ParallaxScript implements IScript {

	private Entity item;

	float ratioX, ratioY;
	float positionX, positionY, playerX, playerY;
	private Game game;
	boolean firstTime;

	public ParallaxScript(float pRatioX, float pRatioY, Game game) {
		ratioX = pRatioX;
		ratioY = pRatioY;
		this.game = game;
	}

	@Override
	public void init(Entity item) {
		this.item = item;
		positionX = item.getComponent(TransformComponent.class).x;
		positionY = item.getComponent(TransformComponent.class).y;
	}

	@Override
	public void dispose() {
		dispose();
	}

	@Override
	public void act(float delta) {
		if (firstTime) {
			playerX = game.player.body.getPosition().x;
			playerY = game.player.getY();
			firstTime = true;
		}
		item.getComponent(TransformComponent.class).x = (game.viewport.getCamera().position.x - game.viewport.getScreenWidth() / 2) * ratioX;
		if (ratioY != 0)
			item.getComponent(TransformComponent.class).y = (game.viewport.getCamera().position.y - game.viewport.getScreenHeight() / 2) * ratioY;
	}

}
