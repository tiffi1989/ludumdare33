package com.myld.game;

import com.badlogic.ashley.core.Entity;
import com.uwsoft.editor.renderer.components.physics.PhysicsBodyComponent;
import com.uwsoft.editor.renderer.factory.component.ComponentFactory;
import com.uwsoft.editor.renderer.scripts.IScript;
import com.uwsoft.editor.renderer.utils.ComponentRetriever;

public class FuelScript implements DropScript {

	private PhysicsBodyComponent bodyComponen;
	private boolean firstTime;
	private Game game;
	public Entity entity;

	public FuelScript(Game game) {
		this.game = game;
	}
	
	@Override
	public void init(Entity entity) {
		this.entity = entity;
		bodyComponen = ComponentRetriever.get(entity, PhysicsBodyComponent.class);

	}

	@Override
	public void act(float delta) {
		if(!firstTime){
			bodyComponen.body.setUserData(this);
			firstTime = true;
		}
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}


	@Override
	public void pickup() {
		game.player.addFuel(GameConstants.FUELAMOUNT);
		game.bodiesToRemove.add(bodyComponen.body);
		
	}

	@Override
	public Entity getEntity() {
		return entity;
	}

}
