package com.myld.game;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.math.Vector2;
import com.uwsoft.editor.renderer.components.TransformComponent;
import com.uwsoft.editor.renderer.components.particle.ParticleComponent;
import com.uwsoft.editor.renderer.components.physics.PhysicsBodyComponent;
import com.uwsoft.editor.renderer.data.ParticleEffectVO;
import com.uwsoft.editor.renderer.factory.component.ParticleEffectComponentFactory;
import com.uwsoft.editor.renderer.scripts.IScript;
import com.uwsoft.editor.renderer.utils.ComponentRetriever;

public class Axe implements IScript {
	PhysicsBodyComponent bodyComponent;
	private Game game;
	private Entity particleEntity;
	private boolean firstTime;
	private TransformComponent transform;
	public Entity entity;
	private float timeElapsed;
	public ParticleEffect particle;
	

	public Axe(Game game) {
		this.game = game;
	}

	@Override
	public void init(Entity entity) {
		this.entity = entity;
		particleEntity = EntityCreator.createEntity(ParticleEffectComponentFactory.class, ParticleEffectVO.class, game, "flower");
		bodyComponent = ComponentRetriever.get(entity, PhysicsBodyComponent.class);
		transform = ComponentRetriever.get(entity, TransformComponent.class);
		

	}

	@Override
	public void act(float delta) {
		particle = particleEntity.getComponent(ParticleComponent.class).particleEffect;
		particleEntity.getComponent(TransformComponent.class).x = transform.x+ 100;
		particleEntity.getComponent(TransformComponent.class).y = transform.y + 75;
		timeElapsed += delta;
		if(timeElapsed >= 5)
		game.sceneLoader.engine.removeEntity(particleEntity);
		
	}
	
	
	private boolean isPlayerInRange() {
		Vector2 playerV = game.player.body.getPosition();
		Vector2 catV = bodyComponent.body.getPosition();
		
		if(playerV.dst(catV) < 40f)
			return true;
		else 
			return false;
	}
	

	@Override
	public void dispose() {
	}

}
