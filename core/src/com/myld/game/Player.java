package com.myld.game;

import com.badlogic.ashley.core.Entity;
import com.uwsoft.editor.renderer.scripts.IScript;

import java.awt.Color;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.controllers.Controllers;
import com.badlogic.gdx.controllers.PovDirection;
import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;
import com.badlogic.gdx.graphics.g3d.utils.BaseAnimationController.Transform;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.RayCastCallback;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.Timer.Task;
import com.uwsoft.editor.renderer.components.DimensionsComponent;
import com.uwsoft.editor.renderer.components.LayerMapComponent;
import com.uwsoft.editor.renderer.components.TransformComponent;
import com.uwsoft.editor.renderer.components.particle.ParticleComponent;
import com.uwsoft.editor.renderer.components.physics.PhysicsBodyComponent;
import com.uwsoft.editor.renderer.components.sprite.SpriteAnimationComponent;
import com.uwsoft.editor.renderer.components.sprite.SpriteAnimationStateComponent;
import com.uwsoft.editor.renderer.data.LayerItemVO;
import com.uwsoft.editor.renderer.factory.component.CompositeComponentFactory;
import com.uwsoft.editor.renderer.physics.PhysicsBodyLoader;
import com.uwsoft.editor.renderer.utils.ComponentRetriever;

import box2dLight.Light;
import box2dLight.PointLight;

public class Player implements IScript {

	private World world;
	private Entity player;
	private TransformComponent transformComponent;
	private DimensionsComponent dimensionsComponent;
	private PhysicsBodyComponent bodyComponent;
	private Vector2 speed;
	public Body body;
	private boolean grounded,soundPlayed;
	private Game game;
	private Light light;
	public boolean firstTime, fire,dead;
	private Controller controller;
	private SpriteAnimationStateComponent animationStateComponent;
	private SpriteAnimationComponent animationComponent;
	private PlayerStates playerState;
	private Body fireBody;
	public int fuel = 200;
	public int health = 100;
	public float TimeSinceLastCollison, time456,time1;
	boolean teleportet;

	public Player(Game game) {
		this.world = game.sceneLoader.world;
		this.game = game;
		light = new PointLight(game.sceneLoader.rayHandler, 100);
		light.setColor(com.badlogic.gdx.graphics.Color.WHITE);
		light.setXray(false);
		light.setDistance(20);

		if (controller != null)
			controller = Controllers.getControllers().first();

	}

	@Override
	public void init(Entity entity) {
		player = entity;
		System.out.println("init player");

		transformComponent = ComponentRetriever.get(entity, TransformComponent.class);
		dimensionsComponent = ComponentRetriever.get(entity, DimensionsComponent.class);
		bodyComponent = ComponentRetriever.get(entity, PhysicsBodyComponent.class);
		animationStateComponent = ComponentRetriever.get(entity, SpriteAnimationStateComponent.class);
		animationComponent = ComponentRetriever.get(entity, SpriteAnimationComponent.class);

		speed = new Vector2(20f, 2000f);
		transformComponent.originX = 120;

		BodyDef bodyDef = new BodyDef();
		bodyDef.type = BodyType.DynamicBody;
		bodyDef.gravityScale = 0;
		bodyDef.position.set(100, 100);

		fireBody = game.sceneLoader.world.createBody(bodyDef);

		fireBody.setUserData("fire");
		PolygonShape shape = new PolygonShape();
		shape.setAsBox(3, 1);

		FixtureDef fixtureDef = new FixtureDef();
		fixtureDef.isSensor = true;
		fixtureDef.shape = shape;

		Fixture fixture = fireBody.createFixture(fixtureDef);
		shape.dispose();

	}

	@Override
	public void act(float delta) {
		
		time1 += delta;

		if (!firstTime) {
			bodyComponent.body.setUserData(this);
			body = bodyComponent.body;
			body.setFixedRotation(true);
			firstTime = true;
		}
		TimeSinceLastCollison += delta;
		
		if(health <= 0)
			die();

		if (transformComponent.scaleX < 0)
			game.root.getChild("fireRechts").getComponent(TransformComponent.class).x = transformComponent.x + 100;
		else
			game.root.getChild("fireRechts").getComponent(TransformComponent.class).x = transformComponent.x + 140;

		game.root.getChild("fireRechts").getComponent(TransformComponent.class).y = transformComponent.y + 100;

		controlKeyboard();
		controlController();
		switchAnimations();
		
		
			

		if (transformComponent.scaleX < 0)
			fireBody.setTransform(body.getPosition().x, body.getPosition().y + 5, 0);
		else
			fireBody.setTransform(body.getPosition().x + 12, body.getPosition().y + 5, 0);
		
		if(!fire)
			fireBody.setActive(false);

		light.setPosition(new Vector2(body.getPosition().x +5, body.getPosition().y+5));

		// if (body.getPosition().y < 0)
		// game.showUIStage(true);
		// else
		// game.showUIStage(false);
		
		if(transformComponent.x >= 48000 && transformComponent.y <= -6000 && !teleportet){

			time456 += delta;
			game.loadScene("MainScene");
				
		}
		
		if(time1 > 5 && (health <= 0 || transformComponent.y <= -8000) ){
			System.out.println("hey");
			game.loadScene("MainScene");			
		}
		
		System.out.println(transformComponent.x + " " + transformComponent.y);


	}

	private void die() {
		
	}

	private void switchAnimations() {
		if (fire) {
			switchStates(PlayerStates.FIRE);
			return;
		}

		if (bodyComponent.body.getLinearVelocity().x == 0) {
			switchStates(PlayerStates.STANDING);
		}
		if (bodyComponent.body.getLinearVelocity().x != 0) {
			switchStates(PlayerStates.RUNNING);
		}
	}

	private void switchStates(PlayerStates newState) {
		if (playerState == newState)
			return;
		else {
			playerState = newState;
			switch (newState) {
			case STANDING:
				animationStateComponent.set(animationComponent.frameRangeMap.get("standing"), 24, PlayMode.NORMAL);
				break;
			case RUNNING:
				animationStateComponent.set(animationComponent.frameRangeMap.get("running"), 24, PlayMode.LOOP);
				break;
			case FIRE:
				animationStateComponent.set(animationComponent.frameRangeMap.get("fire"), 24, PlayMode.LOOP);
				break;
			case JUMPING:
				animationStateComponent.set(animationComponent.frameRangeMap.get("jumping"), 24, PlayMode.LOOP);
				break;

			default:
				break;
			}
		}

	}

	private void controlController() {
		if (controller == null)
			return;

		if (controller.getButton(XBoxControllerMapping.BUTTON_X))
			speed.x = 30f;

		if (controller.getButton(XBoxControllerMapping.BUTTON_RB))
			fire();
		else{
			fire = false;
			soundPlayed = false;
		}
			

		switch (controller.getPov(0)) {
		case east:
		case northEast:
		case southEast:
			if (body.getLinearVelocity().x <= speed.x) {
				body.setLinearVelocity(body.getLinearVelocity().x + 2f, body.getLinearVelocity().y);
				transformComponent.scaleX = +Math.abs(transformComponent.scaleX);
			}
			break;
		case northWest:
		case west:
		case southWest:
			if (body.getLinearVelocity().x >= -speed.x) {
				body.setLinearVelocity(body.getLinearVelocity().x - 2f, body.getLinearVelocity().y);
				transformComponent.scaleX = -Math.abs(transformComponent.scaleX);

			}

			break;

		default:
			break;
		}

		if (controller.getButton(XBoxControllerMapping.BUTTON_A) && grounded) {
			body.applyForceToCenter(new Vector2(0, speed.y), true);
			grounded = false;
			SoundHandler.boing.play();
		}

		speed.x = 20f;

	}

	private void controlKeyboard() {
		
		if(Gdx.input.isKeyPressed(Keys.M)){
			System.out.println("PlayerBody:  " + body.getPosition().x + " " + body.getPosition().y);
		}

		if (Gdx.input.isKeyPressed(Keys.CONTROL_LEFT)) {
			fire();
		} else {
			fire = false;
			soundPlayed = false;
			SoundHandler.feueratem.stop();
		}

		if (Gdx.input.isKeyPressed(Keys.SHIFT_LEFT))
			speed.x = 30f;

		if (Gdx.input.isKeyPressed(Keys.LEFT) && body.getLinearVelocity().x >= -speed.x) {
			body.setLinearVelocity(body.getLinearVelocity().x - 2f, body.getLinearVelocity().y);
			transformComponent.scaleX = -Math.abs(transformComponent.scaleX);
		}
		if (Gdx.input.isKeyPressed(Keys.RIGHT) && body.getLinearVelocity().x <= speed.x) {
			body.setLinearVelocity(body.getLinearVelocity().x + 2f, body.getLinearVelocity().y);
			transformComponent.scaleX = +Math.abs(transformComponent.scaleX);
		}

		if (!Gdx.input.isKeyPressed(Keys.RIGHT) && !Gdx.input.isKeyPressed(Keys.LEFT)) {
			if (body.getLinearVelocity().x > 0)
				body.setLinearVelocity(body.getLinearVelocity().x - 1f, body.getLinearVelocity().y);
			if (body.getLinearVelocity().x < 0)
				body.setLinearVelocity(body.getLinearVelocity().x + 1f, body.getLinearVelocity().y);
			if (body.getLinearVelocity().x > -1 && body.getLinearVelocity().x < 1) {
				body.setLinearVelocity(0, body.getLinearVelocity().y);

			}

		}

		if (Gdx.input.isKeyJustPressed(Keys.SPACE) && grounded) {
			body.applyForceToCenter(new Vector2(0, speed.y), true);
			grounded = false;
			SoundHandler.boing.play();
		}

		speed.x = 20f;
	}

	private void fire() {
		if (fuel == 0)
			return;
		
		if(!soundPlayed){
			SoundHandler.feueratem.play();
			soundPlayed = true;
		}

		fireBody.setActive(true);
		fuel--;
		
		game.root.getChild("fireRechts").getComponent(ParticleComponent.class).particleEffect.start();
		float high = 200 * bodyComponent.body.getLinearVelocity().x / 5f;
		if (high < 300 && high > -300) {
			if (transformComponent.scaleX > 0)
				high = 300;
			else
				high = -300;
		}
		game.root.getChild("fireRechts").getComponent(ParticleComponent.class).particleEffect.findEmitter("Untitled").getVelocity().setHigh(high, high);
		;
		game.root.getChild("fireRechts").getComponent(ParticleComponent.class).particleEffect.allowCompletion();
		;
		fire = true;
	}

	public void addFuel(int amount) {
		fuel += amount;
		if (fuel > 200)
			fuel = 200;
		
		health += amount;
		if(health > 100)
			health = 100;
	}
	
	
	

	public void setGrounded() {
		grounded = true;
	}

	public float getWidth() {
		return dimensionsComponent.width;
	}

	public float getX() {
		return transformComponent.x;
	}

	public float getY() {
		return transformComponent.y;
	}

	public boolean isGrounded() {
		return grounded;
	}

	@Override
	public void dispose() {

	}

}
