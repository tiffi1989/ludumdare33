package com.myld.game;

import javax.sound.midi.Soundbank;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g3d.model.Node;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.Timer.Task;
import com.uwsoft.editor.renderer.components.DimensionsComponent;
import com.uwsoft.editor.renderer.components.LayerMapComponent;
import com.uwsoft.editor.renderer.components.NodeComponent;
import com.uwsoft.editor.renderer.components.TransformComponent;
import com.uwsoft.editor.renderer.components.particle.ParticleComponent;
import com.uwsoft.editor.renderer.components.physics.PhysicsBodyComponent;
import com.uwsoft.editor.renderer.components.sprite.AnimationComponent;
import com.uwsoft.editor.renderer.components.sprite.SpriteAnimationComponent;
import com.uwsoft.editor.renderer.components.sprite.SpriteAnimationStateComponent;
import com.uwsoft.editor.renderer.data.FrameRange;
import com.uwsoft.editor.renderer.data.LayerItemVO;
import com.uwsoft.editor.renderer.data.MainItemVO;
import com.uwsoft.editor.renderer.data.ParticleEffectVO;
import com.uwsoft.editor.renderer.data.SimpleImageVO;
import com.uwsoft.editor.renderer.factory.EntityFactory;
import com.uwsoft.editor.renderer.factory.component.ComponentFactory;
import com.uwsoft.editor.renderer.factory.component.ParticleEffectComponentFactory;
import com.uwsoft.editor.renderer.factory.component.SimpleImageComponentFactory;
import com.uwsoft.editor.renderer.scripts.IScript;
import com.uwsoft.editor.renderer.utils.ComponentRetriever;
import com.uwsoft.editor.renderer.utils.ItemWrapper;

import box2dLight.PointLight;
import javafx.scene.Scene;

public class Cat implements MyScript {

	ItemWrapper wrapper;
	private PhysicsBodyComponent bodyComponent;
	private SpriteAnimationStateComponent deadAnimation;
	private SpriteAnimationComponent animationComponent;
	boolean firstTime;
	private Game game;
	public Entity entity;
	private TransformComponent transformComponent;
	public boolean dead;
	private PointLight light;
	private float miauTime, timeElapsed;

	public Cat(Game game) {
		this.game = game;
	}

	@Override
	public void init(Entity entity) {
		this.entity = entity;
		transformComponent = ComponentRetriever.get(entity, TransformComponent.class);

		bodyComponent = ComponentRetriever.get(entity, PhysicsBodyComponent.class);

		deadAnimation = ComponentRetriever.get(entity, SpriteAnimationStateComponent.class);

		animationComponent = ComponentRetriever.get(entity, SpriteAnimationComponent.class);

	}

	@Override
	public void act(float delta) {
		
		timeElapsed += delta;
		
		if (!firstTime) {
			bodyComponent.body.setUserData(this);
			firstTime = true;
		}
		
		if(timeElapsed > 4 && isPlayerInRange() && !dead){
			miauTime += (float) (delta * Math.random());
			if(miauTime > 1){
				SoundHandler.miauMiau.play();
				miauTime = 0;
			}
		}
		
		
		if(dead){
			bodyComponent.body.getFixtureList().first().setRestitution(0);
			if(bodyComponent.body.getLinearVelocity().y <= 1 && bodyComponent.body.getLinearVelocity().y >=-1)
				game.bodiesToRemove.add(bodyComponent.body);

		}
		
		
	}
	
	private boolean isPlayerInRange() {
		Vector2 playerV = game.player.body.getPosition();
		Vector2 catV = bodyComponent.body.getPosition();
		
		if(playerV.dst(catV) < 30f)
			return true;
		else 
			return false;
	}

	public void burn(){
		deadAnimation.set(animationComponent.frameRangeMap.get("burn"), 0, PlayMode.NORMAL);
		animationComponent.fps = 200;

		dead = true;
		
		Entity entity = EntityCreator.createEntity(ParticleEffectComponentFactory.class, ParticleEffectVO.class, game, "fire");
		TransformComponent trans = ComponentRetriever.get(entity, TransformComponent.class);
		trans.x = transformComponent.x + 100;
		trans.y = transformComponent.y + 75;
		
		light = new PointLight(game.sceneLoader.rayHandler, 100);
		light.setColor(com.badlogic.gdx.graphics.Color.WHITE);
		light.setXray(true);
		light.setDistance(20);
		light.setPosition(new Vector2(bodyComponent.body.getPosition().x+5,bodyComponent.body.getPosition().y+5));
		
		SoundHandler.katzetot.play();		
	}
	
	public void drop(){
		Entity entity = EntityCreator.createEntity(SimpleImageComponentFactory.class, SimpleImageVO.class, game, "fuel");
		TransformComponent trans = ComponentRetriever.get(entity, TransformComponent.class);
		trans.x = transformComponent.x;
		trans.y = transformComponent.y + 50;
		ItemWrapper wrapper = new ItemWrapper(entity);
		wrapper.addScript(new FuelScript(game));
	}
	
	public void die(){
		deadAnimation.set(animationComponent.frameRangeMap.get("dead"), 0, PlayMode.NORMAL);
		animationComponent.fps = 200;

		dead = true;
		
		Entity entity = EntityCreator.createEntity(ParticleEffectComponentFactory.class, ParticleEffectVO.class, game, "blood");
		TransformComponent trans = ComponentRetriever.get(entity, TransformComponent.class);
		trans.x = transformComponent.x + 100;
		trans.y = transformComponent.y + 75;
		
		ParticleComponent particle = ComponentRetriever.get(entity, ParticleComponent.class);
		particle.particleEffect.start();
		
		
		Task task = new Task() {
			
			@Override
			public void run() {
//				game.sceneLoader.getEngine().removeEntity(entity);
			}
		};
		Timer.schedule(task, 3);	
		drop();
		SoundHandler.katzetot.play();		

	}
	

	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}

	@Override
	public void setItemWrapper(ItemWrapper wrapper) {
		this.wrapper = wrapper;
	}

}
