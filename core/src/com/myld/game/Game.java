package com.myld.game;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.FPSLogger;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.uwsoft.editor.renderer.SceneLoader;
import com.uwsoft.editor.renderer.components.MainItemComponent;
import com.uwsoft.editor.renderer.components.NodeComponent;
import com.uwsoft.editor.renderer.data.CompositeItemVO;
import com.uwsoft.editor.renderer.data.CompositeVO;
import com.uwsoft.editor.renderer.data.ProjectInfoVO;
import com.uwsoft.editor.renderer.factory.component.ComponentFactory;
import com.uwsoft.editor.renderer.scripts.IScript;
import com.uwsoft.editor.renderer.utils.ComponentRetriever;
import com.uwsoft.editor.renderer.utils.ItemWrapper;

public class Game extends ApplicationAdapter {

	public Viewport viewport;
	public SceneLoader sceneLoader;
	public ItemWrapper root;
	public Player player;
	private Box2DDebugRenderer debugRenderer;
	private CollisionDetection collison;
	private FPSLogger logger;
	private ProjectInfoVO projectInfoVo;
	public ArrayList<Body> bodiesToRemove = new ArrayList<Body>();;
	private boolean restart;
	private float time1;

	@Override
	public void create() {
		Gdx.graphics.setVSync(false);


		viewport = new FitViewport(1280, 720);
		sceneLoader = new SceneLoader();

		collison = new CollisionDetection(this);


		load("MainScene");

		

		logger = new FPSLogger();

	}

	@Override
	public void render() {
		for (Body body : bodiesToRemove) {
//			sceneLoader.world.destroyBody(body);
			
			body.setActive(false);
			
			if(body.getUserData() instanceof DropScript)
				sceneLoader.engine.removeEntity(((DropScript) body.getUserData()).getEntity());
			
		}
		bodiesToRemove.clear();
		Gdx.gl.glClearColor(0 / 255f, 38 / 255f, 70 / 255f, 0);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		logger.log();

		sceneLoader.getEngine().update(Gdx.graphics.getDeltaTime());


		// viewport.getCamera().position.set(player.getX(), viewport.getCamera().position.y, 0);
		viewport.getCamera().position.set(player.getX() + 150, player.getY() + 100, 0);

		if(restart)
			time1 += Gdx.graphics.getDeltaTime();
		
		if(time1 > 5 && restart){
			load("MainScene");
			restart = false;
		}
		
		System.out.println(time1);
	}

	public void loadScene(String sceneName) {
		sceneLoader.engine.removeAllEntities();
		restart = true;
		

	}
	
	public void load(String sceneName){
		sceneLoader.loadScene(sceneName, viewport);
		sceneLoader.world.setContactListener(collison);

		player = new Player(this);
		root = new ItemWrapper(sceneLoader.getRoot());
		
		projectInfoVo = sceneLoader.getRm().getProjectVO();

		root.getChild("player").addScript(player);
		root.getChild("UI").addScript(new UIScript(1, 1, this, root.getChild("UI")));
		root.getChild("background").addScript(new ParallaxScript(0.5f, 0f, this));
//		createEntityFromLibrary("cat", new Cat());	
		for (Entity entity : sceneLoader.engine.getEntities()) {
			if(entity.getComponent(MainItemComponent.class).tags.contains("cat")){
				ItemWrapper itemWrapper = new ItemWrapper(entity);
				Cat cat = new Cat(this);
				cat.setItemWrapper(itemWrapper);
				itemWrapper.addScript(cat);
			}
			if(entity.getComponent(MainItemComponent.class).tags.contains("enemy")){
				ItemWrapper itemWrapper = new ItemWrapper(entity);
				EnemyScript enemy = new EnemyScript(this);
				enemy.setItemWrapper(itemWrapper);
				itemWrapper.addScript(enemy);
			}
			if(entity.getComponent(MainItemComponent.class).tags.contains("bigEnemy")){
				ItemWrapper itemWrapper = new ItemWrapper(entity);
				BigEnemyScript enemy = new BigEnemyScript(this);
				enemy.setItemWrapper(itemWrapper);
				itemWrapper.addScript(enemy);
			}
		}

	}
	
	public Entity createEntityFromLibrary(String name, MyScript script){
		CompositeItemVO compositeItemVO = projectInfoVo.libraryItems.get(name);
				
		Entity entity = sceneLoader.loadFromLibrary(name);
		NodeComponent nodeComponent = ComponentRetriever.get(entity, NodeComponent.class);
		System.out.println(nodeComponent.children);

		ItemWrapper catWrapper = new ItemWrapper(entity);
		MyScript iScript = script;
		script.setItemWrapper(catWrapper);
		catWrapper.addScript(iScript);
		sceneLoader.engine.addEntity(entity);
		
		System.out.println(root.getChild("cat1").getChild("dead").getEntity() + " child");
		
		
		return entity;
		
	}
}
