package com.myld.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;

public class SoundHandler {
	public final static Sound miauMiau = Gdx.audio.newSound(Gdx.files.internal("sounds/miau.wav"));
	public final static Sound axt = Gdx.audio.newSound(Gdx.files.internal("sounds/axt.wav"));
	public final static Sound boing = Gdx.audio.newSound(Gdx.files.internal("sounds/boing.wav"));
	public final static Sound feueratem = Gdx.audio.newSound(Gdx.files.internal("sounds/feueratem.wav"));
	public final static Sound katzetot = Gdx.audio.newSound(Gdx.files.internal("sounds/katzetot.wav"));
	public final static Sound mimimi = Gdx.audio.newSound(Gdx.files.internal("sounds/mimimi.wav"));
	public final static Sound tot = Gdx.audio.newSound(Gdx.files.internal("sounds/tot.wav"));
	public final static Sound au = Gdx.audio.newSound(Gdx.files.internal("sounds/au.wav"));
	public final static Sound katsching = Gdx.audio.newSound(Gdx.files.internal("sounds/katsching.wav"));
}
