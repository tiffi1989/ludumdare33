package com.myld.game;

import com.uwsoft.editor.renderer.scripts.IScript;
import com.uwsoft.editor.renderer.utils.ItemWrapper;

public interface MyScript extends IScript {
	
	void setItemWrapper(ItemWrapper wrapper);

}
