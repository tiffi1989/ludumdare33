package com.myld.game;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Manifold;

public class CollisionDetection implements ContactListener {
	private Game game;

	public CollisionDetection(Game game) {
		this.game = game;
	}

	@Override
	public void beginContact(Contact contact) {
		checkKittyDeath(contact);
		checkKittyGrilled(contact);
		checkEnemyDeath(contact);
		checkEnemyGrilled(contact);
		checkBigEnemyGrilled(contact, false);
		checkGrounded(contact);
		checkForDrops(contact);
		checkForEnemy(contact);
		checkForAxes(contact);

	}

	public void checkForAxes(Contact contact) {
		Body a = contact.getFixtureA().getBody();
		Body b = contact.getFixtureB().getBody();
		Body player = null;
		Body axe = null;
		if (a.getUserData() instanceof Player || b.getUserData() instanceof Player)
			if (a.getUserData() instanceof Axe || b.getUserData() instanceof Axe)
				if (a.getUserData() instanceof Axe) {
					axe = a;
					player = b;
				} else {
					axe = b;
					player = a;
				}
		if (player != null && axe != null) {
			((Axe) axe.getUserData()).particle.getEmitters().first().setContinuous(false);
			if(((Player) player.getUserData()).TimeSinceLastCollison > 0.5f){
				((Player) player.getUserData()).TimeSinceLastCollison = 0;
				((Player) player.getUserData()).health -= 20;
				SoundHandler.au.play();
			}

		}
	}

	public void checkForEnemy(Contact contact) {
		Body a = contact.getFixtureA().getBody();
		Body b = contact.getFixtureB().getBody();
		Body player = null;
		Body axe = null;
		if (a.getUserData() instanceof Player || b.getUserData() instanceof Player)
			if (a.getUserData() instanceof EnemyScript || b.getUserData() instanceof EnemyScript)
				if (a.getUserData() instanceof EnemyScript) {
					axe = a;
					player = b;
				} else {
					axe = b;
					player = a;
				}
		if (player != null && axe != null) {
			if(((Player) player.getUserData()).TimeSinceLastCollison > 0.5f){
				((Player) player.getUserData()).TimeSinceLastCollison = 0;
				((Player) player.getUserData()).health -= 20;
				SoundHandler.au.play();
			}
			
		}
	}

	public void checkForDrops(Contact contact) {
		Body a = contact.getFixtureA().getBody();
		Body b = contact.getFixtureB().getBody();
		Body player = null;
		Body drop = null;
		if (a.getUserData() instanceof Player || b.getUserData() instanceof Player)
			if (a.getUserData() instanceof DropScript || b.getUserData() instanceof DropScript)
				if (a.getUserData() instanceof DropScript) {
					drop = a;
					player = b;
				} else {
					drop = b;
					player = a;
				}
		if (player != null && drop != null) {
			((DropScript) drop.getUserData()).pickup();
			SoundHandler.katsching.play();
		}
	}

	private void checkKittyGrilled(Contact contact) {
		Body a = contact.getFixtureA().getBody();
		Body b = contact.getFixtureB().getBody();
		Body fire = null;
		Body cat = null;
		if (a.getUserData().equals("fire") || b.getUserData().equals("fire"))
			if (a.getUserData() instanceof Cat || b.getUserData() instanceof Cat)
				if (a.getUserData() instanceof Cat) {
					cat = a;
					fire = b;
				} else {
					cat = b;
					fire = a;
				}

		if (fire != null && cat != null) {
			((Cat) cat.getUserData()).burn();
		}

	}

	private void checkKittyDeath(Contact contact) {
		Body a = contact.getFixtureA().getBody();
		Body b = contact.getFixtureB().getBody();
		Body player = null;
		Body cat = null;
		if (a.getUserData() instanceof Player || b.getUserData() instanceof Player)
			if (a.getUserData() instanceof Cat || b.getUserData() instanceof Cat)
				if (a.getUserData() instanceof Cat) {
					cat = a;
					player = b;
				} else {
					cat = b;
					player = a;
				}

		if (player != null && cat != null) {
			if (player.getLinearVelocity().y < 0 && !((Player) player.getUserData()).isGrounded()) {
				((Cat) cat.getUserData()).die();

			}

		}
	}

	private void checkBigEnemyGrilled(Contact contact, boolean end) {
		Body a = contact.getFixtureA().getBody();
		Body b = contact.getFixtureB().getBody();
		Body fire = null;
		Body enemy = null;
		if (a.getUserData().equals("fire") || b.getUserData().equals("fire"))
			if (a.getUserData() instanceof BigEnemyScript || b.getUserData() instanceof BigEnemyScript)
				if (a.getUserData() instanceof BigEnemyScript) {
					enemy = a;
					fire = b;
				} else {
					enemy = b;
					fire = a;
				}

		if (fire != null && enemy != null) {
			if(!end)
			((BigEnemyScript) enemy.getUserData()).startBurn();
			else
				((BigEnemyScript) enemy.getUserData()).endBurn();
		}

	}
	private void checkEnemyGrilled(Contact contact) {
		Body a = contact.getFixtureA().getBody();
		Body b = contact.getFixtureB().getBody();
		Body fire = null;
		Body enemy = null;
		if (a.getUserData().equals("fire") || b.getUserData().equals("fire"))
			if (a.getUserData() instanceof EnemyScript || b.getUserData() instanceof EnemyScript)
				if (a.getUserData() instanceof EnemyScript) {
					enemy = a;
					fire = b;
				} else {
					enemy = b;
					fire = a;
				}
		
		if (fire != null && enemy != null) {
			((EnemyScript) enemy.getUserData()).burn();
		}
		
	}

	private void checkEnemyDeath(Contact contact) {
		Body a = contact.getFixtureA().getBody();
		Body b = contact.getFixtureB().getBody();
		Body player = null;
		Body enemy = null;
		if (a.getUserData() instanceof Player || b.getUserData() instanceof Player)
			if (a.getUserData() instanceof EnemyScript || b.getUserData() instanceof EnemyScript)
				if (a.getUserData() instanceof EnemyScript) {
					enemy = a;
					player = b;
				} else {
					enemy = b;
					player = a;
				}

		if (player != null && enemy != null) {
			if (player.getLinearVelocity().y < 0 && !((Player) player.getUserData()).isGrounded()) {
				if(!(enemy.getUserData() instanceof BigEnemyScript)){
					((EnemyScript) enemy.getUserData()).die();
					System.out.println("tot");
				}
				

			}

		}
	}

	public void checkGrounded(Contact contact) {
		Body a = contact.getFixtureA().getBody();
		Body b = contact.getFixtureB().getBody();

		if ((a.getUserData() instanceof Player || b.getUserData() instanceof Player) && !(b.getUserData() instanceof Cat || a.getUserData() instanceof Cat))
			game.player.setGrounded();
	}

	@Override
	public void endContact(Contact contact) {
		checkBigEnemyGrilled(contact, true);
	}

	@Override
	public void preSolve(Contact contact, Manifold oldManifold) {
		// TODO Auto-generated method stub

	}

	@Override
	public void postSolve(Contact contact, ContactImpulse impulse) {

	}

}
