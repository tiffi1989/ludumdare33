package com.myld.game;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.utils.StringBuilder;
import com.uwsoft.editor.renderer.components.TransformComponent;
import com.uwsoft.editor.renderer.components.label.LabelComponent;
import com.uwsoft.editor.renderer.scripts.IScript;
import com.uwsoft.editor.renderer.utils.ItemWrapper;

public class UIScript implements IScript {

	private Entity item;

	float ratioX, ratioY;
	float positionX, positionY, playerX, playerY;
	private Game game;
	boolean firstTime;
	private Entity fuelBar,fuelBarLabel;
	private Entity healthBar,healthBarLabel;
	private ItemWrapper compositeItem;

	public UIScript(float pRatioX, float pRatioY, Game game, ItemWrapper compositeItem) {
		ratioX = pRatioX;
		ratioY = pRatioY;
		this.game = game;
		this.compositeItem = compositeItem;
	}

	@Override
	public void init(Entity item) {
		this.item = item;
		fuelBar = compositeItem.getChild("fuelBar").getChild("bar").getEntity();
		fuelBarLabel = compositeItem.getChild("fuelBar").getChild("label").getEntity();
		healthBar = compositeItem.getChild("healthBar").getChild("bar").getEntity();
		healthBarLabel = compositeItem.getChild("healthBar").getChild("label").getEntity();
		positionX = item.getComponent(TransformComponent.class).x;
		positionY = item.getComponent(TransformComponent.class).y;
	}

	@Override
	public void dispose() {
		dispose();
	}

	@Override
	public void act(float delta) {
		if (firstTime) {
			playerX = game.player.body.getPosition().x;
			playerY = game.player.getY();
			firstTime = true;
		}
		fuelBar.getComponent(TransformComponent.class).scaleX = game.player.fuel/200f;
		int labelLength = fuelBarLabel.getComponent(LabelComponent.class).text.length;
		fuelBarLabel.getComponent(LabelComponent.class).text.replace(0, labelLength, "Fuel: " + game.player.fuel/2f + "%");
		healthBar.getComponent(TransformComponent.class).scaleX = game.player.health/100f;
		int healthLabelLength = healthBarLabel.getComponent(LabelComponent.class).text.length;
		healthBarLabel.getComponent(LabelComponent.class).text.replace(0, healthLabelLength, "Health: " + game.player.health + "%");
		
		item.getComponent(TransformComponent.class).x = (game.viewport.getCamera().position.x - game.viewport.getScreenWidth() / 2) * ratioX;
		if (ratioY != 0)
			item.getComponent(TransformComponent.class).y = (game.viewport.getCamera().position.y - game.viewport.getScreenHeight() / 2) * ratioY;
	}

}
