package com.myld.game;

import java.lang.reflect.InvocationTargetException;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.physics.box2d.World;
import com.uwsoft.editor.renderer.data.MainItemVO;
import com.uwsoft.editor.renderer.data.ParticleEffectVO;
import com.uwsoft.editor.renderer.factory.component.ComponentFactory;
import com.uwsoft.editor.renderer.factory.component.ParticleEffectComponentFactory;
import com.uwsoft.editor.renderer.resources.IResourceRetriever;

import box2dLight.RayHandler;

public class EntityCreator {

	public static <T extends ComponentFactory, Y extends MainItemVO> Entity createEntity(Class<T> componentClass, Class<Y> mainItemVO, Game game, String childName) {
		Entity entity = new Entity();
		try {

			MainItemVO vo = mainItemVO.newInstance();
			vo.loadFromEntity(game.root.getChild(childName).getEntity());
			ComponentFactory factory;
			factory = componentClass.getConstructor(RayHandler.class, World.class, IResourceRetriever.class).newInstance(game.sceneLoader.rayHandler, game.sceneLoader.world, game.sceneLoader.getRm());
			factory.createComponents(game.root.getEntity(), entity, (MainItemVO) vo);
			game.sceneLoader.engine.addEntity(entity);
		} catch (InstantiationException e) {

			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return entity;

	}
}
